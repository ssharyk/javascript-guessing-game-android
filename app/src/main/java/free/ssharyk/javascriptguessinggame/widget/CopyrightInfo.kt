package free.ssharyk.javascriptguessinggame.widget

import androidx.compose.Composable
import androidx.ui.core.Text
import androidx.ui.core.dp
import androidx.ui.foundation.Clickable
import androidx.ui.graphics.Color
import androidx.ui.layout.Column
import androidx.ui.layout.ExpandedWidth
import androidx.ui.layout.Padding
import androidx.ui.text.ParagraphStyle
import androidx.ui.text.TextStyle
import androidx.ui.text.style.TextAlign
import androidx.ui.text.style.TextDecoration
import free.ssharyk.javascriptguessinggame.model.Data

@Composable
fun CopyrightInfo(onClick: () -> Unit) {


    Padding(left = 32.dp, top = 12.dp, right = 32.dp, bottom = 12.dp) {

        Column {

            Text(
                modifier = ExpandedWidth,

                text = "See web origin:",

                style = TextStyle(
                    color = Color.Green
                ),

                paragraphStyle = ParagraphStyle(
                    textAlign = TextAlign.Center
                )
            )

            Clickable(onClick) {
                Text(
                    modifier = ExpandedWidth,

                    text = Data.WEB_ORIGIN,

                    style = TextStyle(
                        color = Color(0x05, 0xc5, 0xcf),
                        decoration = TextDecoration.Underline
                    ),

                    paragraphStyle = ParagraphStyle(
                        textAlign = TextAlign.Center
                    )
                )

            }
        }
    }
}