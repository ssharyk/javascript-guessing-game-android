package free.ssharyk.javascriptguessinggame.widget

import android.content.Context
import android.content.res.Configuration.ORIENTATION_LANDSCAPE
import android.content.res.Configuration.ORIENTATION_PORTRAIT
import androidx.compose.Composable
import androidx.compose.state
import androidx.compose.unaryPlus
import androidx.ui.core.dp
import androidx.ui.layout.*
import free.ssharyk.javascriptguessinggame.gameplay.GameController
import free.ssharyk.javascriptguessinggame.model.Answer

interface IMainScreenInteractor {
    val context: Context

    fun onCopyrightClick()
}

@Composable
fun MainScreen(interactor: IMainScreenInteractor?, gameController: GameController) {

    val currentLevel = +state { gameController.nextLevel() }
    val answers = currentLevel.value.options.map { Answer(it.name) }

    var orientation = interactor?.context?.resources?.configuration?.orientation
        ?: ORIENTATION_PORTRAIT
    if (orientation == ORIENTATION_PORTRAIT && (interactor?.context?.resources?.displayMetrics?.widthPixels ?: 0) <= 720)
        orientation = ORIENTATION_LANDSCAPE

    Container(modifier = ExpandedHeight) {

        Column {

            CopyrightInfo() {
                interactor?.onCopyrightClick()
            }

            Center {
                Column(
                    modifier = Spacing(16.dp)
                ) {

                    LevelInfo(currentLevel.value.number)

                    interactor?.let {
                        Container(modifier = ExpandedWidth) {
                            Logo(interactor.context, currentLevel.value.framework)
                        }
                    }

                    HeightSpacer(16.dp)

                    AnswerOptions(orientation, answers) {
                        if (it.text == currentLevel.value.framework.name) {
                            // OK - just to next level
                        } else {
                            // error - reset to start and the 1st level
                            gameController.reset()
                        }

                        currentLevel.value = gameController.nextLevel()
                    }
                }
            }
        }
    }
}