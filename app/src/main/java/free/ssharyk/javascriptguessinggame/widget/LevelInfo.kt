package free.ssharyk.javascriptguessinggame.widget

import androidx.compose.Composable
import androidx.ui.core.Text
import androidx.ui.graphics.Color
import androidx.ui.layout.ExpandedWidth
import androidx.ui.text.ParagraphStyle
import androidx.ui.text.TextStyle
import androidx.ui.text.style.TextAlign

@Composable
fun LevelInfo(level: Int) {
    Text(
        modifier = ExpandedWidth,

        text = "Level: $level",

        style = TextStyle(
            color = Color.Green
        ),

        paragraphStyle = ParagraphStyle(
            textAlign = TextAlign.Center
        )
    )
}