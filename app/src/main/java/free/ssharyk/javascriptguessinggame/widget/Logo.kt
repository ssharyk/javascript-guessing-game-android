package free.ssharyk.javascriptguessinggame.widget

import android.content.Context
import androidx.compose.Composable
import androidx.ui.core.Clip
import androidx.ui.core.dp
import androidx.ui.foundation.DrawImage
import androidx.ui.foundation.shape.corner.RoundedCornerShape
import androidx.ui.graphics.imageFromResource
import androidx.ui.layout.Container
import androidx.ui.layout.Height
import androidx.ui.layout.Width
import free.ssharyk.javascriptguessinggame.model.Framework

@Composable
fun Logo(context: Context, framework: Framework) {

    val drawableResourceId: Int =
        context.resources.getIdentifier(framework.name, "drawable", context.packageName)

    Container(modifier = Height(100.dp) wraps Width(100.dp)) {
        Clip(shape = RoundedCornerShape(8.dp)) {
            DrawImage(
                imageFromResource(context.resources, drawableResourceId)
            )
        }
    }

}