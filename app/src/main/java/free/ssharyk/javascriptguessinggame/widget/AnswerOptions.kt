package free.ssharyk.javascriptguessinggame.widget

import android.content.res.Configuration
import androidx.compose.Composable
import androidx.ui.core.Alignment
import androidx.ui.core.dp
import androidx.ui.foundation.shape.border.Border
import androidx.ui.foundation.shape.corner.RoundedCornerShape
import androidx.ui.graphics.Color
import androidx.ui.layout.*
import androidx.ui.material.Button
import androidx.ui.material.OutlinedButtonStyle
import free.ssharyk.javascriptguessinggame.model.Answer

@Composable
fun AnswerOptions(orientation: Int, answers: List<Answer>, onItemSelected: (Answer) -> Unit) {

    val rowsCount = if (orientation == Configuration.ORIENTATION_LANDSCAPE) 4 else 2
    val columnsCount = if (orientation == Configuration.ORIENTATION_LANDSCAPE) 1 else 2

    Container(modifier = Spacing(16.dp)) {
        Table(columns = columnsCount, alignment = {
            Alignment.Center
        }) {
            for (row in 0 until rowsCount) {
                tableRow {
                    for (column in 0 until columnsCount) {
                        val answer = answers[row * columnsCount + column]
                        SingleAnswerOption(answer, onItemSelected)
                    }
                }
            }
        }
    }

}

@Composable
fun SingleAnswerOption(answer: Answer, onItemSelected: (Answer) -> Unit) {

    Container(expanded = true) {

        Padding(padding = 16.dp) {
            Align(alignment = Alignment.Center) {
                Button(
                    modifier = ExpandedWidth,

                    text = answer.text.toUpperCase(),

                    style = OutlinedButtonStyle(
                        color = Color.Transparent,
                        border = Border(color = Color.Green, width = 1.dp),
                        shape = RoundedCornerShape(50), //50% percent
                        contentColor = Color.Green,
                        elevation = 4.dp
                    ),

                    onClick = {
                        onItemSelected(answer)
                    }
                )
            }
        }

    }

}