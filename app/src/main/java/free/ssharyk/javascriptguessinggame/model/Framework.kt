package free.ssharyk.javascriptguessinggame.model

data class Framework(
    val name: String,
    var wasUsedForGame: Boolean = false
)