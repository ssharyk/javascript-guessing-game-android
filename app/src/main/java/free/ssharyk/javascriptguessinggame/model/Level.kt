package free.ssharyk.javascriptguessinggame.model

data class Level(
    val number: Int,
    val framework: Framework,
    val options: List<Framework>
)