package free.ssharyk.javascriptguessinggame.model

inline class Answer(
    val text: String
)