package free.ssharyk.javascriptguessinggame

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.Composable
import androidx.compose.unaryPlus
import androidx.ui.core.Clip
import androidx.ui.core.dp
import androidx.ui.core.setContent
import androidx.ui.foundation.DrawImage
import androidx.ui.foundation.VerticalScroller
import androidx.ui.foundation.shape.corner.RoundedCornerShape
import androidx.ui.graphics.Color
import androidx.ui.layout.*
import androidx.ui.material.ColorPalette
import androidx.ui.material.MaterialTheme
import androidx.ui.material.surface.Surface
import androidx.ui.res.imageResource
import androidx.ui.tooling.preview.Preview
import free.ssharyk.javascriptguessinggame.gameplay.GameController
import free.ssharyk.javascriptguessinggame.model.Answer
import free.ssharyk.javascriptguessinggame.model.Data
import free.ssharyk.javascriptguessinggame.widget.*


class MainActivity : AppCompatActivity(), IMainScreenInteractor {

    private val gameController = GameController()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme(
                colors = ColorPalette(
                    surface = Color(0x21, 0x1, 0x21),
                    background = Color(0x21, 0x1, 0x21),
                    primary = Color(0xBB, 0x86, 0xFC),
                    secondary = Color(0x03, 0xDA, 0xC6)

                )
            ) {

                Surface(color = Color(0x21, 0x1, 0x21)) {
                    VerticalScroller {
                        MainScreen(this@MainActivity, gameController)
                    }
                }
            }
        }
    }

    override val context: Context
        get() = this

    override fun onCopyrightClick() {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(Data.WEB_ORIGIN))
        startActivity(browserIntent)
    }
}

@Preview
@Composable
fun DefaultPreview() {
    val level = GameController().nextLevel()
    val answers = level.options.map { Answer(it.name) }

    MaterialTheme(
        colors = ColorPalette(
            surface = Color(0x21, 0x1, 0x21),
            background = Color(0x21, 0x1, 0x21),
            primary = Color(0xBB, 0x86, 0xFC),
            secondary = Color(0x03, 0xDA, 0xC6)
        )
    ) {

        Surface(color = Color(0x21, 0x1, 0x21)) {
            Container(modifier = ExpandedHeight) {

                Column {

                    CopyrightInfo() {}

                    Center {
                        Column(
                            modifier = Spacing(16.dp)
                        ) {

                            LevelInfo(12)

                            HeightSpacer(16.dp)

                            Container(modifier = ExpandedWidth) {

                                Container(modifier = Height(100.dp) wraps Width(100.dp)) {
                                    Clip(shape = RoundedCornerShape(8.dp)) {
                                        DrawImage(image = +imageResource(R.drawable.react))
                                    }
                                }

                            }

                            HeightSpacer(16.dp)

                            AnswerOptions(Configuration.ORIENTATION_LANDSCAPE, answers) {}
                        }
                    }

                }
            }

        }
    }
}