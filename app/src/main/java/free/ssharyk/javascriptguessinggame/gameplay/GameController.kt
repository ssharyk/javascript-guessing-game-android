package free.ssharyk.javascriptguessinggame.gameplay

import free.ssharyk.javascriptguessinggame.model.Framework
import free.ssharyk.javascriptguessinggame.model.Level

class GameController {

    private var level: Int = 0

    /**
     * Prepare data for the next level
     */
    fun nextLevel(): Level {
        val framework = randomFramework()

        return Level(
            ++level,
            framework,
            listOf(framework, randomFramework(), randomFramework(), randomFramework()).shuffled()
        )
    }

    /**
     * Move to the first level again
     */
    fun reset() {
        this.level = 0
        FrameworkRepository.reset()
    }

    private fun randomFramework(): Framework =
        FrameworkRepository.getNew()

}